import EventEmitter from './EventEmitter';
import hasPassiveEvents from './utils/hasPassiveEvents';

export interface DelegateEventListener<T extends Event = Event> {
  (event: DelegatedEvent<T>): void;
}

export interface DelegateMap {
  [event: string]: DelegateEventListener;
}

export interface DelegateOptions {
  capture?: boolean;
  passive?: boolean;
  selector?: string;
  scope?: any;
}

export interface DelegateListener {
  capture: boolean;
  element: Element | Document | Window;
  eventName: string;
  handler: EventListener;
  listener: DelegateEventListener;
  scope: any;
  selector: string | undefined;
}

export type DelegatedEvent<T extends Event = Event> = T & {
  delegateTarget: HTMLElement;
};

export default class Delegate extends EventEmitter {
  delegates: Array<DelegateListener> = [];
  element: Element | Document | Window;

  constructor(element: Element | Document | Window) {
    super();
    this.element = element;
  }

  dispose() {
    super.dispose();
    this.undelegateAll();
  }

  delegate<T extends Event = Event>(
    eventName: string,
    listener: DelegateEventListener<T>,
    options: DelegateOptions = {}
  ): EventListener {
    const { capture = false, selector, scope = this } = options;
    const { element } = this;
    let handler: EventListener;

    if (selector) {
      handler = function(event: any) {
        let node = <HTMLElement>(event.target || event.srcElement);
        for (; node && node != element; node = <HTMLElement>node.parentNode) {
          if (!node.matches) break;
          if (!node.matches(selector)) continue;
          event.delegateTarget = node;
          listener.call(scope, event);
          break;
        }
      };
    } else {
      handler = function(event: any) {
        listener.call(scope, event);
      };
    }

    if (options.passive === false && hasPassiveEvents()) {
      element.addEventListener(eventName, handler, <any>{
        capture,
        passive: false,
      });
    } else {
      element.addEventListener(eventName, handler, capture);
    }

    this.delegates.push({
      capture,
      element,
      eventName,
      handler,
      listener: listener as any,
      scope,
      selector,
    });

    return handler;
  }

  delegateEvents(events: DelegateMap, options?: DelegateOptions) {
    Object.keys(events).forEach(key => {
      this.delegate(key, events[key], options);
    });
  }

  undelegate<T extends Event = Event>(
    eventName: string,
    listener?: DelegateEventListener<T>,
    options: DelegateOptions = {}
  ): this {
    const { delegates } = this;
    const { capture, selector, scope } = options;
    const handlers = delegates.slice();
    let index = handlers.length;

    while (index--) {
      const handler = handlers[index];
      if (
        handler.eventName === eventName &&
        (capture ? handler.capture === capture : true) &&
        (listener ? handler.listener === listener : true) &&
        (scope ? handler.scope === scope : true) &&
        (selector ? handler.selector === selector : true)
      ) {
        delegates.splice(index, 1);
        handler.element.removeEventListener(
          handler.eventName,
          handler.handler,
          capture
        );
      }
    }

    return this;
  }

  undelegateAll(): this {
    const { delegates } = this;
    for (let index = 0, count = delegates.length; index < count; index++) {
      const { element, eventName, handler } = delegates[index];
      element.removeEventListener(eventName, handler, false);
    }

    delegates.length = 0;
    return this;
  }

  undelegateEvents(events: DelegateMap, options?: DelegateOptions) {
    Object.keys(events).forEach(key => {
      this.undelegate(key, events[key], options);
    });
  }
}

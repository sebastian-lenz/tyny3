export type CustomEventOptions = {
  [key: string]: any;
  type: string;
};

export type CustomEvent<
  TOptions extends CustomEventOptions,
  TTarget = any
> = Event<TTarget> &
  {
    [K in keyof TOptions]: TOptions[K];
  };

export interface EventOptions<TTarget = any> {
  target: TTarget;
  type: string;
}

export default class Event<TTarget = any> {
  readonly type: string;
  readonly target: TTarget;

  private _defaultPrevented: boolean = false;
  private _propagationStopped: boolean = false;

  constructor(options: EventOptions<TTarget>) {
    this.target = options.target;
    this.type = options.type;
  }

  isDefaultPrevented(): boolean {
    return this._defaultPrevented;
  }

  isPropagationStopped(): boolean {
    return this._propagationStopped;
  }

  preventDefault() {
    this._defaultPrevented = true;
  }

  stopPropagation() {
    this._propagationStopped = true;
  }
}

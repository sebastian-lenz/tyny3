import components from './services/components';
import createElement, { CreateElementOptions } from './utils/createElement';
import Delegate, { DelegatedEvent } from './Delegate';
import Params from './Params';
import PointerList from './pointers/PointerList';
import viewport from './services/viewport';
import { IntervalType } from './types';

function ensureElement(options: ViewOptions): HTMLElement {
  return !options.element ? createElement(options) : options.element;
}

export { DelegatedEvent };

export interface ViewClass<TView extends View = View> {
  new (options: any): TView;
}

export interface ViewOptions extends CreateElementOptions {
  [name: string]: any;
  element?: HTMLElement;
  parent?: View;
}

export interface CreateChildOptions<T extends View> {
  options?: any;
  selector: string;
  viewClass: ViewClass<T>;
}

export default class View extends Delegate {
  children: Array<View> = [];
  element!: HTMLElement;
  parent: View | null;
  pointerList: PointerList | null = null;

  static classNamePrefix: string = 'tyny';

  constructor(options: ViewOptions = {}) {
    super(ensureElement(options));

    const parent = options.parent || null;
    this.parent = parent;
    if (parent) {
      parent.children.push(this);
    }
  }

  dispose() {
    super.dispose();
    const { children, parent, element, pointerList } = this;

    const allChildren = children.slice();
    children.length = 0;

    for (let index = 0, count = allChildren.length; index < count; index++) {
      allChildren[index].dispose();
    }

    const scope = parent ? parent.children : components().views;
    const index = scope.indexOf(this);
    if (index !== -1) {
      scope.splice(index, 1);
    }

    if (pointerList) {
      pointerList.dispose();
    }

    if (element && element.parentNode) {
      element.parentNode.removeChild(element);
    }

    this.parent = null;
    this.pointerList = null;
  }

  getParams(options: ViewOptions) {
    return new Params(this, options, this.element);
  }

  getVisibilityBounds(): IntervalType {
    const { element } = this;
    const bounds = element.getBoundingClientRect();
    const min = bounds.top + viewport().scrollTop;
    const max = min + (bounds.height || element.offsetHeight);
    return { min, max };
  }

  handleResize() {
    const { children } = this;
    for (let index = 0, count = children.length; index < count; index++) {
      children[index].handleResize();
    }
  }

  query<T extends HTMLElement = HTMLElement>(selectors: string): T {
    return <T>this.element.querySelector(selectors);
  }

  queryAll<T extends HTMLElement = HTMLElement>(selectors: string): T[] {
    return Array.prototype.slice.call(this.element.querySelectorAll(selectors));
  }

  // Child utilities
  // ---------------

  createChild<T extends View>({
    options = {},
    selector,
    viewClass,
  }: CreateChildOptions<T>): T {
    return new viewClass({
      ...options,
      element: this.query(selector),
      parent: this,
    });
  }

  createChildren<T extends View>({
    options = {},
    selector,
    viewClass,
  }: CreateChildOptions<T>): T[] {
    return this.queryAll(selector).map(
      element => new viewClass({ ...options, element, parent: this })
    );
  }

  createOptionalChild<T extends View>({
    options = {},
    selector,
    viewClass,
  }: CreateChildOptions<T>): T | null {
    const element = this.query(selector);
    if (!element) {
      return null;
    }

    return new viewClass({
      ...options,
      element,
      parent: this,
    });
  }

  // Class name utilities
  // --------------------

  addClass(...tokens: string[]): this {
    this.element.classList.add(...tokens);
    return this;
  }

  hasClass(token: string): boolean {
    return this.element.classList.contains(token);
  }

  removeClass(...tokens: string[]): this {
    this.element.classList.remove(...tokens);
    return this;
  }

  toggleClass(token: string, force?: boolean): boolean {
    const { classList } = this.element;

    return typeof force === 'boolean'
      ? classList.toggle(token, force)
      : classList.toggle(token);
  }
}

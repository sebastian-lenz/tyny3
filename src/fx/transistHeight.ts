import transistDimensions from './transistDimensions';
import { TransistOptions, TransistPropertyMap } from './transist';

export interface TransistHeightOptions extends Partial<TransistOptions> {
  extraProperties?: TransistPropertyMap;
}

export default function transistHeight(
  element: HTMLElement,
  callback: Function,
  options: TransistHeightOptions = {}
): Promise<void> {
  return transistDimensions(element, callback, {
    transistHeight: true,
    ...options,
  });
}

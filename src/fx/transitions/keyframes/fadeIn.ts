import keyframes from '../../keyframes';
import memoize from '../../../utils/memoize';

export default memoize(function fadeIn(): string {
  return keyframes('tynyFadeInKeyframes', {
    from: {
      opacity: '0',
    },
    to: {
      opacity: '1',
    },
  });
});

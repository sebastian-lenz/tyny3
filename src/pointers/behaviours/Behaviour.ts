import PointerList from '../PointerList';
import EventEmitter from '../../EventEmitter';

export default class Behaviour extends EventEmitter {
  list: PointerList;

  constructor(list: PointerList) {
    super();
    this.list = list;
  }
}

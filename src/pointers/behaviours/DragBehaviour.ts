import Behaviour from './Behaviour';
import PointerList from '../PointerList';
import PointerListEvent from '../PointerListEvent';
import DragEvent from './DragEvent';

let dragCounter = 0;
document.addEventListener(
  'touchforcechange',
  (e) => (dragCounter ? e.preventDefault() : null),
  { passive: false }
);

export type WatchMode = 'idle' | 'listening' | 'draging';

export default class DragBehaviour extends Behaviour {
  direction: 'horizontal' | 'vertical' | 'both' = 'both';
  disabled: boolean = false;
  threshold: number = 2;
  protected watchMode: WatchMode = 'idle';

  constructor(list: PointerList) {
    super(list);

    this.listenTo(list, PointerListEvent.addEvent, this.handlePointerAdd);
    this.listenTo(list, PointerListEvent.removeEvent, this.handlePointerRemove);
    this.listenTo(list, PointerListEvent.updateEvent, this.handlePointerUpdate);
  }

  protected emitDragEvent(type: string, listEvent: PointerListEvent): boolean {
    const event = new DragEvent({
      listEvent,
      pointerList: listEvent.target,
      target: this,
      type,
    });

    this.emit(event);
    return event.isDefaultPrevented();
  }

  protected handlePointerAdd = (event: PointerListEvent) => {
    if (this.isDisabled()) {
      return;
    }

    if (this.watchMode !== 'idle') {
      event.preventDefault();
    } else {
      this.watchMode = 'listening';
    }
  };

  protected handlePointerRemove = (event: PointerListEvent) => {
    const { watchMode } = this;
    this.setWatchMode('idle');

    if (watchMode === 'draging') {
      this.emitDragEvent(DragEvent.dragEndEvent, event);
    } else if (watchMode === 'listening') {
      this.emitDragEvent(DragEvent.clickEvent, event);
    }
  };

  protected handlePointerUpdate = (event: PointerListEvent) => {
    const { watchMode } = this;
    const { pointer, nativeEvent } = event;
    if (nativeEvent) nativeEvent.preventDefault();

    if (watchMode === 'listening') {
      const { direction, threshold } = this;
      if (pointer.getDeltaLength() < threshold) return;

      const { x, y } = pointer.getDelta();
      if (
        (direction === 'horizontal' && Math.abs(x) < Math.abs(y)) ||
        (direction === 'vertical' && Math.abs(x) > Math.abs(y)) ||
        this.emitDragEvent(DragEvent.dragStartEvent, event)
      ) {
        this.setWatchMode('idle');
        return;
      }

      pointer.resetInitialPosition();
      this.setWatchMode('draging');
    }

    if (watchMode === 'draging') {
      this.emitDragEvent(DragEvent.dragEvent, event);

      if (event.isDefaultPrevented()) {
        this.setWatchMode('idle');
        return;
      }
    }
  };

  protected isDisabled(): boolean {
    return this.disabled;
  }

  protected setWatchMode(value: WatchMode) {
    const { watchMode } = this;
    if (watchMode === value) return;

    if (value === 'draging') {
      dragCounter += 1;
    } else if (watchMode === 'draging') {
      dragCounter -= 1;
    }

    this.watchMode = value;
  }
}

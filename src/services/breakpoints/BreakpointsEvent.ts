import Breakpoints from './Breakpoints';
import Event, { EventOptions } from '../../Event';
import { Breakpoint } from './index';

export interface BreakpointsEventOptions extends EventOptions<Breakpoints> {
  breakpoint: Breakpoint;
}

export default class BreakpointsEvent extends Event<Breakpoints> {
  readonly breakpoint: Breakpoint;

  static readonly changeEvent: string = 'change';

  constructor(options: BreakpointsEventOptions) {
    super(options);
    this.breakpoint = options.breakpoint;
  }
}

import createViews from './createViews';
import setIdleCallback from '../../utils/setIdleCallback';
import whenDomReady from '../../utils/whenDomReady';
import viewport, { ViewportEvent } from '../viewport';
import View, { ViewClass } from '../../View';

export interface Component {
  selector: string;
  viewClass: ViewClass;
}

export default class Components {
  isUpdateQueued: boolean = false;
  readonly components: Component[] = [];
  readonly pendingComponents: Component[] = [];
  readonly views: View[] = [];

  constructor() {
    const { views } = this;

    viewport().on(ViewportEvent.resizeEvent, () => {
      for (let index = 0, count = views.length; index < count; index++) {
        views[index].handleResize();
      }
    });
  }

  createComponents(view: View) {
    return createViews({
      components: this.components,
      existingViews: view.children,
      rootElement: view.element,
    });
  }

  initializeComponents() {
    if (this.isUpdateQueued) return;
    this.isUpdateQueued = true;

    whenDomReady().then(() => {
      setIdleCallback(() => {
        const components = this.pendingComponents.slice();
        this.pendingComponents.length = 0;
        this.isUpdateQueued = false;

        const views = createViews({
          components,
          existingViews: this.views,
        });

        this.views.push(...views);
        if (views.length) {
          viewport().triggerResize();
        }
      });
    });
  }

  registerComponent(component: Component) {
    const { components, pendingComponents } = this;
    components.push(component);
    pendingComponents.push(component);

    this.initializeComponents();
  }
}

import View, { ViewClass, ViewOptions } from '../../View';
import { Component } from './Components';

export interface CreateViewsOptions {
  components: Component[];
  existingViews?: Array<View>;
  options?: Partial<ViewOptions>;
  rootElement?: HTMLElement;
}

export default function createViews({
  components,
  existingViews = [],
  options = {},
  rootElement = document.body,
}: CreateViewsOptions): Array<View> {
  let scheduledViews: Array<{
    element: HTMLElement;
    path: Array<HTMLElement>;
    viewClass: ViewClass;
  }> = [];

  function pushComponent({ selector, viewClass }: Component) {
    const elements = rootElement.querySelectorAll(selector);
    const count = elements.length;

    for (let index = 0; index < count; index++) {
      pushView(viewClass, elements[index] as HTMLElement);
    }
  }

  function pushView(viewClass: ViewClass, element: HTMLElement) {
    const path = toPath(element);
    if (
      existingViews.some(view => path.indexOf(view.element) !== -1) ||
      scheduledViews.some(view => path.indexOf(view.element) !== -1)
    ) {
      return;
    }

    scheduledViews = [
      ...scheduledViews.filter(view => view.path.indexOf(element) === -1),
      {
        element,
        path,
        viewClass,
      },
    ];
  }

  function toPath(element: HTMLElement) {
    const path: Array<HTMLElement> = [];
    while (element.parentElement && element.parentElement !== rootElement) {
      element = element.parentElement;
      path.push(element);
    }

    return path.reverse();
  }

  for (let index = 0, count = components.length; index < count; index++) {
    pushComponent(components[index]);
  }

  return scheduledViews.map(
    ({ element, viewClass }) => new viewClass({ ...options, element })
  );
}

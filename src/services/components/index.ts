import Components from './Components';

let instance: Components;

export default function components(): Components {
  if (!instance) instance = new Components();
  return instance;
}

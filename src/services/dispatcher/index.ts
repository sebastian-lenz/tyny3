import Dispatcher from './Dispatcher';
import DispatcherEvent from './DispatcherEvent';

let instance: Dispatcher;

export { DispatcherEvent };

export default function dispatcher(): Dispatcher {
  if (!instance) instance = new Dispatcher();
  return instance;
}

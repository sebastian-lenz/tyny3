import FontObserver from './FontObserver';

let instance: FontObserver;

export default function fontObserver(): FontObserver {
  if (!instance) instance = new FontObserver();
  return instance;
}

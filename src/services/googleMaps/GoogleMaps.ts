/**
 * Loader callback definition.
 */
export type GoogleMapsLoaderCallback = { (): void };

/**
 * Defines the possible load states.
 */
export const enum GoogleMapsState {
  Idle,
  Loading,
  Loaded,
}

export interface GoogleMapsLoadOptions {
  apiKey?: string;
  url?: string;
  query?: string;
}

/**
 * A service class that loads the GoogleMaps API.
 */
export default class GoogleMaps {
  /**
   * The API key that should be used.
   */
  apiKey: string | undefined;

  /**
   * Deferred object for creating load promises.
   */
  private promise: Promise<typeof google.maps> | null = null;

  /**
   * Current state of the API loader.
   */
  private state: GoogleMapsState = GoogleMapsState.Idle;

  /**
   * Return the current load state of the loader.
   *
   * @returns
   *   The current load state of the loader.
   */
  getState(): GoogleMapsState {
    return this.state;
  }

  /**
   * Load the google maps api.
   */
  load({
    apiKey = this.apiKey,
    url = 'https://maps.googleapis.com/maps/api/js',
    query = '',
  }: GoogleMapsLoadOptions = {}): Promise<typeof google.maps> {
    let { promise } = this;
    if (!promise) {
      promise = new Promise(resolve => {
        (<any>window)['onTynyGoogleMapsReady'] = () => {
          this.state = GoogleMapsState.Loaded;
          resolve(google.maps);
        };
      });

      const script = document.createElement('script');
      script.src = `${url}?key=${apiKey}&callback=onTynyGoogleMapsReady${query}`;

      (document.head || document.body).appendChild(script);

      this.promise = promise;
      this.state = GoogleMapsState.Loading;
    }

    return promise;
  }
}

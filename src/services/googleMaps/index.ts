import GoogleMaps from './GoogleMaps';

let instance: GoogleMaps;

export default function googleMaps(): GoogleMaps {
  if (!instance) instance = new GoogleMaps();
  return instance;
}

import Delegate from '../../Delegate';
import dispatcher, { DispatcherEvent } from '../dispatcher';
import ViewportEvent from './ViewportEvent';

const isCSS1Compat = (document.compatMode || '') === 'CSS1Compat';
const supportsPageOffset = 'pageXOffset' in window;

function pointerDown() {
  if ('PointerEvent' in window) {
    return 'pointerdown';
  } else if ('ontouchstart' in window) {
    return 'touchstart';
  }

  return 'mousedown';
}

export default class Viewport extends Delegate {
  height: number = 0;
  scrollBarSize: number = Number.NaN;
  scrollLeft: number = 0;
  scrollTop: number = 0;
  width: number = 0;
  private hasScrollChanged: boolean = false;
  private hasSizeChanged: boolean = false;
  private isInitialized: boolean = false;
  private scrollInitiators: any[] = [];
  private scrollRestorePosition: number = 0;

  constructor() {
    super(window);

    this.delegate('resize', this.handleResize);
    this.delegate('scroll', this.handleScroll);
    this.delegate(pointerDown(), this.handlePointerDown);
    this.listenTo(dispatcher(), DispatcherEvent.frameEvent, this.handleFrame);
  }

  disableScrollbars(initiator: any) {
    const { scrollInitiators } = this;
    let { scrollBarSize } = this;

    const index = scrollInitiators.indexOf(initiator);
    if (index === -1) {
      scrollInitiators.push(initiator);
    }

    if (scrollInitiators.length === 1) {
      const { body } = document;
      const { style } = body;
      const { scrollTop } = this;

      if (isNaN(scrollBarSize)) {
        const { offsetWidth } = body;
        style.width = '100%';
        style.position = 'fixed';
        scrollBarSize = body.offsetWidth - offsetWidth;
        this.scrollBarSize = scrollBarSize;
      } else {
        style.width = '100%';
        style.position = 'fixed';
      }

      style.paddingRight = `${scrollBarSize}px`;
      style.top = `-${scrollTop}px`;

      this.scrollRestorePosition = scrollTop;
      this.handleResize();
      this.emit(
        new ViewportEvent({
          target: this,
          type: ViewportEvent.scrollbarsEvent,
        })
      );
    }
  }

  enableScrollbars(initiator: any) {
    const { scrollInitiators } = this;
    const index = scrollInitiators.indexOf(initiator);
    if (index !== -1) {
      scrollInitiators.splice(index, 1);
    }

    if (scrollInitiators.length === 0) {
      const { style } = document.body;
      style.position = '';
      style.paddingRight = '';
      style.top = '';
      style.width = '';
      window.scrollTo(this.scrollLeft, this.scrollRestorePosition);

      this.handleResize();
      this.emit(
        new ViewportEvent({
          target: this,
          type: ViewportEvent.scrollbarsEvent,
        })
      );
    }
  }

  handleFrame() {
    if (!this.isInitialized) {
      this.handleResize();
      this.handleScroll();
      this.hasSizeChanged = true;
      this.hasScrollChanged = true;
      this.isInitialized = true;
      return;
    }

    if (this.hasSizeChanged) {
      this.hasSizeChanged = false;
      this.emit(
        new ViewportEvent({
          target: this,
          type: ViewportEvent.measureEvent,
        })
      );

      this.emit(
        new ViewportEvent({
          target: this,
          type: ViewportEvent.resizeEvent,
        })
      );
    }

    if (this.hasScrollChanged) {
      this.hasScrollChanged = false;
      this.emit(
        new ViewportEvent({
          target: this,
          type: ViewportEvent.scrollEvent,
        })
      );
    }
  }

  handlePointerDown(nativeEvent: Event) {
    this.emit(
      new ViewportEvent({
        nativeEvent,
        target: this,
        type: ViewportEvent.pointerDownEvent,
      })
    );
  }

  handleResize() {
    const doc = document.documentElement;
    const width = Math.max(doc ? doc.clientWidth : 0, 0);
    const height = Math.max(doc ? doc.clientHeight : 0, 0);

    if (this.width != width || this.height != height) {
      this.width = width;
      this.height = height;
      this.hasSizeChanged = true;
    }
  }

  handleScroll() {
    const doc = document.documentElement || document.body;
    let scrollLeft = 0;
    let scrollTop = 0;

    if (!this.hasScrollbars()) {
      return;
    }

    if (supportsPageOffset) {
      scrollLeft = window.pageXOffset;
      scrollTop = window.pageYOffset;
    } else if (isCSS1Compat && doc) {
      scrollLeft = doc.scrollLeft;
      scrollTop = doc.scrollTop;
    } else {
      scrollLeft = document.body.scrollLeft;
      scrollTop = document.body.scrollTop;
    }

    if (this.scrollLeft != scrollLeft || this.scrollTop != scrollTop) {
      this.scrollLeft = scrollLeft;
      this.scrollTop = scrollTop;
      this.hasScrollChanged = true;
    }
  }

  hasScrollbars(): boolean {
    return this.scrollInitiators.length === 0;
  }

  setScrollLeft(value: number) {
    if (this.scrollLeft === value) return;
    window.scrollTo(value, this.scrollTop);
  }

  setScrollTop(value: number) {
    if (this.scrollTop === value) return;
    window.scrollTo(this.scrollLeft, value);
  }

  toggleScrollbars(
    initiator: any,
    value: boolean = this.scrollInitiators.indexOf(initiator) !== -1
  ): boolean {
    if (value) {
      this.enableScrollbars(initiator);
    } else {
      this.disableScrollbars(initiator);
    }

    return value;
  }

  triggerResize() {
    this.hasSizeChanged = true;
  }
}

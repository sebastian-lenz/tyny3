import Viewport from './Viewport';
import ViewportEvent from './ViewportEvent';

let instance: Viewport;

export { ViewportEvent };

export default function viewport(): Viewport {
  if (!instance) instance = new Viewport();
  return instance;
}

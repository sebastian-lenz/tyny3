import NativeObserver, { NativeTarget } from './NativeObserver';
import VisibilityHandler from './VisibilityHandler';
import { VisibilityTarget } from '.';
import View from '../../View';

export default class NativeHandler extends VisibilityHandler<NativeObserver> {
  readonly intersections: IntersectionObserver;
  readonly rootMargin: string = '75% 0% 75% 0%';

  constructor() {
    super();

    this.intersections = new IntersectionObserver(this.handleIntersections, {
      rootMargin: this.rootMargin,
    });
  }

  handleIntersection({ isIntersecting, target }: IntersectionObserverEntry) {
    const { observers } = this;
    for (let index = 0; index < observers.length; index++) {
      const observer = observers[index];
      if (observer.target.element === target) {
        observer.target.setInViewport(isIntersecting);
      }
    }
  }

  handleIntersections = (entries: IntersectionObserverEntry[]) => {
    for (let index = 0; index < entries.length; index++) {
      this.handleIntersection(entries[index]);
    }
  };

  register(target: NativeTarget): NativeObserver {
    const observer = new NativeObserver({
      handler: this,
      target,
    });

    this.observers.push(observer);
    this.intersections.observe(target.element);
    return observer;
  }

  unregister(target: VisibilityTarget) {
    if (target instanceof View) {
      this.intersections.unobserve(target.element);
    }

    super.unregister(target);
  }
}

import ScriptObserver from './ScriptObserver';
import viewport, { ViewportEvent } from '../viewport';
import VisibilityHandler from './VisibilityHandler';
import { IntervalType } from '../../types';
import { VisibilityTarget } from './index';

export default class ScriptHandler extends VisibilityHandler<ScriptObserver>
  implements IntervalType {
  bleed: number = 500;
  max: number = Number.NaN;
  min: number = Number.NaN;

  constructor() {
    super();

    this.listenTo(
      viewport(),
      ViewportEvent.measureEvent,
      this.handleViewportEvent,
      -10
    );

    this.listenTo(
      viewport(),
      ViewportEvent.scrollEvent,
      this.handleViewportEvent,
      -10
    );
  }

  register(target: VisibilityTarget): ScriptObserver {
    const { observers } = this;
    let observer = this.getObserver(target);

    if (!observer) {
      if (isNaN(this.max) || isNaN(this.min)) {
        this.update();
      }

      observer = new ScriptObserver({ handler: this, target });
      observer.update();
      observers.push(observer);
    }

    return observer;
  }

  update(): void {
    const { bleed, observers } = this;
    const { scrollTop, height } = viewport();
    const min = scrollTop - bleed;
    const max = scrollTop + height + bleed;

    if (this.max !== max || this.min !== min) {
      this.min = min;
      this.max = max;
      observers.forEach(observer => observer.update());
    }
  }

  updateTarget(target: VisibilityTarget): void {
    const observer = this.getObserver(target);
    if (observer) {
      observer.update();
    }
  }

  handleViewportEvent() {
    this.update();
  }
}

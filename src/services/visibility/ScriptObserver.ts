import Interval from '../../types/Interval';
import ScriptHandler from './ScriptHandler';
import { Observer } from './Visibility';
import { VisibilityTarget } from './index';

export interface ScriptObserverOptions {
  handler: ScriptHandler;
  target: VisibilityTarget;
}

export default class ScriptObserver extends Interval implements Observer {
  readonly target: VisibilityTarget;
  protected isVisible: boolean = false;
  protected handler: ScriptHandler;

  constructor(options: ScriptObserverOptions) {
    super(0, 0);

    this.target = options.target;
    this.handler = options.handler;
  }

  update(): void {
    const { target, handler: visibility } = this;
    const { max, min } = target.getVisibilityBounds();
    this.min = min;
    this.max = max;

    const isVisible = this.intersects(visibility);
    if (this.isVisible !== isVisible) {
      this.isVisible = isVisible;
      target.setInViewport(isVisible);
    }
  }
}

import NativeHandler from './NativeHandler';
import ScriptHandler from './ScriptHandler';
import View from '../../View';
import VisibilityHandler from './VisibilityHandler';
import { VisibilityTarget } from './index';

export interface Observer {
  target: VisibilityTarget;
  update(): void;
}

export default class Visibility {
  isNativeHandlerSupported: boolean;
  nativeHandler: NativeHandler | null = null;
  scriptHandler: ScriptHandler | null = null;

  constructor() {
    this.isNativeHandlerSupported = typeof IntersectionObserver === 'function';
  }

  getHandlers(): Array<VisibilityHandler> {
    const result = [];
    if (this.nativeHandler) result.push(this.nativeHandler);
    if (this.scriptHandler) result.push(this.scriptHandler);
    return result;
  }

  getNativeHandler() {
    if (!this.nativeHandler) this.nativeHandler = new NativeHandler();
    return this.nativeHandler;
  }

  getObserver(target: VisibilityTarget): Observer | undefined {
    return this.getHandlers().reduce(
      (observer, handler) => observer || handler.getObserver(target),
      undefined as Observer | undefined
    );
  }

  getScriptHandler() {
    if (!this.scriptHandler) this.scriptHandler = new ScriptHandler();
    return this.scriptHandler;
  }

  register(target: VisibilityTarget): Observer {
    if (this.isNativeHandlerSupported && target instanceof View) {
      return this.getNativeHandler().register(target);
    }

    return this.getScriptHandler().register(target);
  }

  unregister(target: VisibilityTarget): void {
    this.getHandlers().forEach(handler => handler.unregister(target));
  }

  update(): void {
    this.getHandlers().forEach(handler => handler.update());
  }

  updateTarget(target: VisibilityTarget): void {
    const observer = this.getObserver(target);
    if (observer) {
      observer.update();
    }
  }
}

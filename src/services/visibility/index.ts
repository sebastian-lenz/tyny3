import Visibility from './Visibility';
import { IntervalType } from '../../types';

export interface VisibilityTarget {
  getVisibilityBounds(): IntervalType;
  setInViewport(value: boolean): void;
}

let instance: Visibility;

export default function visibility(): Visibility {
  if (!instance) instance = new Visibility();
  return instance;
}

export interface CreateElementOptions {
  appendTo?: HTMLElement;
  attributes?: { [name: string]: string };
  className?: string;
  prependTo?: HTMLElement;
  tagName?: string;
  template?: string | Function;
}

export default function createElement(
  options: CreateElementOptions
): HTMLElement {
  const {
    appendTo,
    attributes,
    className,
    prependTo,
    tagName = 'div',
    template,
  } = options;

  const element = document.createElement(tagName);

  if (appendTo) {
    appendTo.appendChild(element);
  } else if (prependTo) {
    prependTo.insertBefore(element, prependTo.firstElementChild);
  }

  if (className) {
    element.className = className;
  }

  if (attributes) {
    Object.keys(attributes).forEach(key => {
      element.setAttribute(key, attributes[key]);
    });
  }

  if (template) {
    if (typeof template === 'function') {
      element.innerHTML = template(options);
    } else {
      element.innerHTML = template;
    }
  }

  return element;
}

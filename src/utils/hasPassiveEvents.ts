let _hasPassiveEvents: boolean | undefined;

export default function hasPassiveEvents(): boolean {
  if (_hasPassiveEvents === void 0) {
    _hasPassiveEvents = false;
    try {
      const listener = () => null;
      const opts = Object.defineProperty({}, 'passive', {
        get: () => (_hasPassiveEvents = true),
      });

      window.addEventListener('test', listener, opts);
      window.removeEventListener('test', listener, opts);
    } catch (e) {}
  }

  return _hasPassiveEvents;
}

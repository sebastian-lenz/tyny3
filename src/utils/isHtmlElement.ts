export default function isHtmlElement(value: any): value is HTMLElement {
  return value && typeof value === 'object' && value.nodeType === 1;
}

import View from '../View';

export interface LoadableView extends View {
  load(): Promise<void>;
}

export default function isLoadableView(view: any): view is LoadableView {
  return view && view instanceof View && 'load' in view;
}

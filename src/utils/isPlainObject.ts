import isObject from './isObject';

function isObjectObject(value: any): value is Object {
  return (
    isObject(value) === true &&
    Object.prototype.toString.call(value) === '[object Object]'
  );
}

export interface PlainObject {
  [name: string]: any;
}

export default function isPlainObject(value: any): value is PlainObject {
  var ctor, prot;

  if (isObjectObject(value) === false) return false;

  // If has modified constructor
  ctor = value.constructor;
  if (typeof ctor !== 'function') return false;

  // If has modified prototype
  prot = ctor.prototype;
  if (isObjectObject(prot) === false) return false;

  // If constructor does not have an Object-specific method
  if (prot.hasOwnProperty('isPrototypeOf') === false) {
    return false;
  }

  // Most likely a plain Object
  return true;
}

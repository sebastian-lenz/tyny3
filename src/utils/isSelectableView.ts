import View from '../View';

export interface SelectableView extends View {
  setSelected(value: boolean): void;
}

export default function isSelectableView(view: any): view is SelectableView {
  return view && view instanceof View && 'setSelected' in view;
}

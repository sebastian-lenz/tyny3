import 'native-promise-only';

import './animationFrame';
import './array';
import './classList';
import './eventListener';
import './matches';
import './object';
import './string';
import './symbol';

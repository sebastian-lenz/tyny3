import isPlainObject, { PlainObject } from './isPlainObject';

function encode(params: PlainObject): string {
  return Object.keys(params)
    .map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
    })
    .join('&');
}

export interface RequestOptions {
  method?: 'DELETE' | 'GET' | 'POST' | 'PUT';
  headers?: any;
  init?: (xhr: XMLHttpRequest) => void;
  params?: Document | BodyInit | PlainObject | null;
  url: string;
}

export default function request({
  method = 'GET',
  headers,
  init,
  params,
  url,
}: RequestOptions): Promise<string> {
  return new Promise(function (resolve, reject) {
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);

    xhr.onload = function () {
      if (this.status >= 200 && this.status < 300) {
        resolve(`${xhr.response}`);
      } else {
        reject({
          status: this.status,
          statusText: xhr.statusText,
        });
      }
    };

    xhr.onerror = function () {
      reject({
        status: this.status,
        statusText: xhr.statusText,
      });
    };

    if (headers && typeof headers === 'object') {
      Object.keys(headers).forEach(function (key) {
        xhr.setRequestHeader(key, headers[key]);
      });
    }

    if (init) {
      init(xhr);
    }

    xhr.send(isPlainObject(params) ? encode(params) : params);
  });
}

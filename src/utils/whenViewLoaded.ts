import isLoadableView from './isLoadableView';

export default function whenViewLoaded(view: any): Promise<void> {
  return isLoadableView(view) ? view.load() : Promise.resolve();
}

import CycleableView, { CycleableViewEvent } from '../CycleableView/index';
import View, { ViewOptions } from '../../View';

export type ArrowTargetOptions =
  | {
      target: CycleableView;
    }
  | {
      parent: CycleableView;
    };

export type ArrowsOptions = ViewOptions & ArrowTargetOptions & {};

export default class Arrows extends View {
  backward: HTMLButtonElement;
  forward: HTMLButtonElement;
  readonly target: CycleableView;

  constructor(options: ArrowsOptions) {
    super({
      className: `${View.classNamePrefix}Arrows`,
      ...options,
    });

    this.target = options.target ? options.target : options.parent;
    const params = this.getParams(options);

    this.backward = params.element<HTMLButtonElement>({
      appendTo: this.element,
      className: 'backward',
      defaultValue: '.backward',
      name: 'backward',
      tagName: 'button',
    });

    this.forward = params.element<HTMLButtonElement>({
      appendTo: this.element,
      className: 'forward',
      defaultValue: '.forward',
      name: 'forward',
      tagName: 'button',
    });

    this.delegate('click', this.handleClick);
    this.listenTo(
      options.target,
      CycleableViewEvent.changeEvent,
      this.handleChange
    );
  }

  handleChange() {
    const { backward, forward, target } = this;
    if (target.isLooped) return;

    const index = target.getCurrentIndex();
    backward.disabled = index <= 0;
    forward.disabled = index >= target.getLength() - 1;
  }

  handleClick(event: Event) {
    const { backward, forward } = this;
    let target = event.target as HTMLElement | null;

    while (target) {
      if (target === backward) {
        return this.navigate(-1);
      } else if (target === forward) {
        return this.navigate(1);
      }

      target = target.parentElement;
    }
  }

  navigate(step: number) {
    const { target } = this;
    target.setCurrentIndex(target.getCurrentIndex() + step);
  }
}

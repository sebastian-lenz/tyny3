import CycleableView from './index';
import Event, { EventOptions } from '../../Event';
import View from '../../View';

export interface CycleableViewEventOptions extends EventOptions<CycleableView> {
  fromView: View | null;
  options: any;
  toView: View | null;
}

export default class CycleableViewEvent extends Event<CycleableView> {
  readonly fromView: View | null;
  readonly options: any;
  readonly toView: View | null;

  static changeEvent: string = 'change';

  constructor(options: CycleableViewEventOptions) {
    super(options);

    this.fromView = options.fromView;
    this.options = options.options;
    this.toView = options.toView;
  }
}

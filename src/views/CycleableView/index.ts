import ListView, { ListViewOptions } from '../ListView';
import CycleableViewEvent from './CycleableViewEvent';
import View from '../../View';

export { CycleableViewEvent };

export interface CycleableViewOptions extends ListViewOptions {
  initialIndex?: number;
  isLooped?: boolean;
}

export default class CycleableView<
  TItem extends View = View,
  TTransitionOptions = any
> extends ListView<TItem> {
  current: TItem | null = null;
  isLooped: boolean;

  constructor(options: CycleableViewOptions = {}) {
    super(options);

    const { initialIndex, isLooped = false } = options;
    this.isLooped = isLooped;

    if (initialIndex !== void 0) {
      this.setCurrentIndex(initialIndex);
    }
  }

  getCurrentIndex(): number {
    const { current } = this;
    return current ? this.indexOf(current) : -1;
  }

  getNext(): TItem | null {
    return this.getItem(this.normalizeIndex(this.getCurrentIndex() + 1));
  }

  getPrevious(): TItem | null {
    return this.getItem(this.normalizeIndex(this.getCurrentIndex() - 1));
  }

  handleItemRemove(child: TItem, index: number) {
    if (child === this.current) {
      this.setCurrent(null);
    }
  }

  handleTransition(
    from: TItem | null,
    to: TItem | null,
    options?: TTransitionOptions
  ) {}

  normalizeIndex(index: number): number {
    const count = this.getLength();
    if (count < 1) {
      return -1;
    }

    let normalized = index;
    if (this.isLooped) {
      while (normalized < 0) normalized += count;
      while (normalized >= count) normalized -= count;
    } else {
      if (normalized < 0) return -1;
      if (normalized >= count) return -1;
    }

    return normalized;
  }

  setCurrent(item: TItem | null, options?: TTransitionOptions): this {
    const { current: fromView } = this;
    if (fromView === item) {
      return this;
    }

    this.current = item;
    this.handleTransition(fromView, item, options);

    this.emit(
      new CycleableViewEvent({
        fromView,
        options,
        target: this,
        toView: item,
        type: CycleableViewEvent.changeEvent,
      })
    );

    return this;
  }

  setCurrentImmediate(item: TItem) {
    this.setCurrent(item);
  }

  setCurrentIndex(index: number, options?: TTransitionOptions): this {
    index = this.normalizeIndex(index);
    return this.setCurrent(this.getItem(index), options);
  }
}

import Dropdown from './index';
import DropdownOption from './DropdownOption';
import Event, { EventOptions } from '../../Event';

export interface DropdownEventOptions extends EventOptions<Dropdown> {
  option: DropdownOption | null;
}

export default class DropdownEvent extends Event<Dropdown> {
  readonly option: DropdownOption | null;

  static change: string = 'change';

  constructor(options: DropdownEventOptions) {
    super(options);
    this.option = options.option;
  }
}

import Dropdown from './index';
import DropdownOption from './DropdownOption';
import View, { ViewOptions } from '../../View';

export interface DropdownLabelOptions extends ViewOptions {
  emptyLabel?: string;
}

export default class DropdownLabel extends View {
  emptyLabel: string;
  extraClass: string | null = null;
  option: DropdownOption | null = null;
  parent!: Dropdown;

  constructor(options: DropdownLabelOptions) {
    super({
      className: `${View.classNamePrefix}DropdownLabel`,
      ...options,
    });

    const params = this.getParams(options);
    this.emptyLabel = params.string({
      name: 'emptyLabel',
      defaultValue: '-',
    });

    this.delegate('click', this.handleClick);
  }

  setExtraClass(extraClass: string) {
    if (this.extraClass === extraClass) return;
    if (this.extraClass) this.removeClass(this.extraClass);

    this.extraClass = extraClass;
    if (extraClass) this.addClass(extraClass);
  }

  setOption(option: DropdownOption | null) {
    if (this.option === option) return;

    const { element, emptyLabel } = this;
    element.innerHTML = option ? option.caption : emptyLabel;

    this.setExtraClass(option ? option.extraClass : 'empty');
    this.option = option;
  }

  handleClick(event: Event) {
    event.preventDefault();
    this.parent.onLabelClick();
  }
}

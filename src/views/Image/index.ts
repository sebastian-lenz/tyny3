import ImageEvent from './ImageEvent';
import isImageLoaded from '../../utils/isImageLoaded';
import SourceSet, { SourceSetMode, SourceSetSource } from './SourceSet';
import View, { ViewOptions } from '../../View';
import visibility, { VisibilityTarget } from '../../services/visibility';
import viewport, { ViewportEvent } from '../../services/viewport';

export { SourceSet, SourceSetMode, SourceSetSource };

export interface ImageOptions extends ViewOptions {
  disableMeasure?: boolean;
  disableVisibility?: boolean;
  height?: number;
  sourceSet?: SourceSet | SourceSetSource;
  width?: number;
}

export default class Image extends View implements VisibilityTarget {
  currentSource: string | null = null;
  displayHeight: number = Number.NaN;
  displayWidth: number = Number.NaN;
  element!: HTMLImageElement;
  isInViewport: boolean = false;
  loadPromise: Promise<void>;
  naturalHeight: number;
  naturalWidth: number;
  sourceSet: SourceSet;

  constructor(options: ImageOptions) {
    super({
      className: `${View.classNamePrefix}Image`,
      ...options,
      tagName: 'img',
    });

    const params = this.getParams(options);

    this.naturalHeight = params.int({
      attribute: 'height',
      defaultValue: Number.NaN,
      name: 'height',
    });

    this.naturalWidth = params.int({
      attribute: 'width',
      defaultValue: Number.NaN,
      name: 'width',
    });

    this.sourceSet = params.instance({
      attribute: 'data-srcset',
      ctor: SourceSet,
      name: 'sourceSet',
    });

    this.loadPromise = this.createPromise();

    if (!options.disableVisibility) {
      visibility().register(this);
    }

    if (!options.disableMeasure) {
      this.listenTo(viewport(), ViewportEvent.measureEvent, this.handleMeasure);
    }
  }

  dispose() {
    super.dispose();
    visibility().unregister(this);
  }

  handleMeasure() {
    const { element } = this;
    this.displayWidth = element.offsetWidth;
    this.displayHeight = element.offsetHeight;
  }

  handleResize() {
    this.update();
  }

  load(): Promise<void> {
    this.isInViewport = true;
    this.update();
    return this.loadPromise;
  }

  private createPromise() {
    const { element } = this;

    return new Promise(resolve => {
      const handleLoad = () => {
        this.undelegate('load', handleLoad);
        resolve();
      };

      if (isImageLoaded(element)) {
        resolve();
      } else {
        this.delegate('load', handleLoad);
      }
    }).then(() => {
      const { naturalHeight, naturalWidth } = element;

      if (isNaN(this.naturalHeight)) {
        this.naturalHeight = naturalHeight;
      }

      if (isNaN(this.naturalWidth)) {
        this.naturalWidth = naturalWidth;
      }

      element.classList.add('loaded');
      this.emit(
        new ImageEvent({
          target: this,
          type: ImageEvent.loadEvent,
        })
      );
    });
  }

  setInViewport(inViewport: boolean) {
    if (this.isInViewport === inViewport) return;
    this.isInViewport = inViewport;

    if (inViewport) {
      this.update();
    }
  }

  setDisplaySize(width: number, height: number) {
    this.displayHeight = height;
    this.displayWidth = width;
    this.update();
  }

  toOptions(): ImageOptions {
    return {
      height: this.naturalHeight,
      sourceSet: this.sourceSet,
      width: this.naturalWidth,
    };
  }

  update() {
    if (isNaN(this.displayWidth)) {
      this.handleMeasure();
    }

    const { displayWidth, element, isInViewport, sourceSet } = this;
    if (isInViewport && sourceSet) {
      const source = sourceSet.get(displayWidth);
      if (this.currentSource !== source) {
        this.currentSource = source;
        element.src = source;
      }
    }
  }
}

import dissolve from '../../fx/transitions/dissolve';
import debounce from '../../utils/debounce';
import Params from '../../Params';
import Ratio, { RatioOptions } from './Ratio';
import RatioSet, { RatioSetSource } from './RatioSet';
import Swap, { SwapOptions } from '../Swap';
import viewport, { ViewportEvent } from '../../services/viewport';
import View from '../../View';
import visibility, { VisibilityTarget } from '../../services/visibility';
import { ImageCrop, CropMode, ImageCropEvent } from '../ImageCrop';

export { Ratio, RatioOptions, RatioSet, RatioSetSource };

export interface ImageRatiosOptions extends SwapOptions {
  disableMeasure?: boolean;
  disableVisibility?: boolean;
  ratioSet?: RatioSet | RatioSetSource;
}

export interface SourceData {
  height: number;
  sourceSet: string;
  width: number;
}

export default class ImageRatios extends Swap<ImageCrop>
  implements VisibilityTarget {
  debouncedUpdate: Function;
  displayHeight: number = 0;
  displayWidth: number = 0;
  inViewport: boolean = false;
  isLoaded: boolean = false;
  ratioSet: RatioSet;

  constructor(options: ImageRatiosOptions) {
    super({
      className: `${View.classNamePrefix}ImageRatios`,
      transition: dissolve({ duration: 200, noPureFadeIn: true }),
      ...options,
      appendContent: true,
      disposeContent: true,
      transist: null,
    });

    this.ratioSet = this.getRatioSet(this.getParams(options));
    this.debouncedUpdate = debounce(this.update, 50);
    console.log(this.ratioSet);

    if (!options.disableVisibility) {
      visibility().register(this);
    }

    if (!options.disableMeasure) {
      this.listenTo(viewport(), ViewportEvent.measureEvent, this.handleMeasure);
    }
  }

  dispose() {
    super.dispose();
    visibility().unregister(this);
  }

  getRatioSet(params: Params): RatioSet {
    const cropOptions = {
      disableMaskResize: params.bool({ name: 'disableMaskResize' }),
      focusY: params.number({ name: 'focusY' }),
      focusX: params.number({ name: 'focusX' }),
      maxScale: params.number({ name: 'maxScale' }),
      minScale: params.number({ name: 'minScale' }),
      mode: params.enum({
        name: 'mode',
        enum: CropMode,
        defaultValue: CropMode.Cover,
      }),
    };

    return new RatioSet(
      this.readSources().map(
        (source) =>
          new Ratio({
            ...cropOptions,
            ...source,
          })
      )
    );
  }

  handleLoad() {
    if (this.isLoaded) return;
    this.isLoaded = true;
    this.addClass('loaded');
  }

  handleMeasure() {
    const { element } = this;
    this.displayHeight = element.offsetHeight;
    this.displayWidth = element.offsetWidth;
  }

  handleResize() {
    const { content, displayHeight, displayWidth } = this;
    if (content) {
      content.setDisplaySize(displayWidth, displayHeight);
    }

    super.handleResize();
    this.debouncedUpdate();
  }

  load(): Promise<void> {
    this.inViewport = true;
    this.update();

    if (!this.content) {
      return Promise.resolve();
    }

    return this.content.load();
  }

  readSources(): Array<SourceData> {
    return this.queryAll('source')
      .map((source) => {
        const height =
          source.getAttribute('data-height') || source.getAttribute('height');
        const sourceSet =
          source.getAttribute('data-srcset') || source.getAttribute('srcset');
        const width =
          source.getAttribute('data-width') || source.getAttribute('width');

        if (!height || !sourceSet || !width) {
          return null;
        }

        return {
          height: parseInt(height),
          sourceSet,
          width: parseInt(width),
        } as SourceData;
      })
      .filter(
        (source: SourceData | null): source is SourceData => source !== null
      );
  }

  setDisplaySize(width: number, height: number) {
    this.displayHeight = height;
    this.displayWidth = width;

    const { content } = this;
    if (content) {
      content.setDisplaySize(width, height);
    }

    this.update();
  }

  setInViewport(inViewport: boolean) {
    if (this.inViewport === inViewport) return;
    this.inViewport = inViewport;
    this.update();

    const { content } = this;
    if (content) {
      content.setInViewport(inViewport);
    }
  }

  update() {
    const {
      content,
      displayHeight,
      displayWidth,
      element,
      inViewport,
      ratioSet,
    } = this;

    if (!inViewport) return;

    const crop = ratioSet.get(displayWidth, displayHeight);
    if (crop && (!content || content.crop !== crop)) {
      const image = new ImageCrop({
        appendTo: element,
        crop,
        disableMeasure: true,
        disableVisibility: true,
        parent: this,
        imageOptions: {
          height: crop.height,
          sourceSet: crop.sourceSet,
          width: crop.width,
        },
      });

      image.setDisplaySize(displayWidth, displayHeight);

      if (content) this.stopListening(content);
      this.listenTo(image, ImageCropEvent.loadEvent, this.handleLoad);
      this.setContent(image);
    }
  }
}

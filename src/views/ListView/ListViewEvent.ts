import ListView from './index';
import Event, { EventOptions } from '../../Event';
import View from '../../View';

export interface ListViewEventOptions extends EventOptions<ListView> {
  index: number;
  item: View;
}

export default class ListViewEvent extends Event<ListView> {
  readonly index: number;
  readonly item: View;

  static addEvent: string = 'add';
  static removeEvent: string = 'remove';

  constructor(options: ListViewEventOptions) {
    super(options);

    this.index = options.index;
    this.item = options.item;
  }
}

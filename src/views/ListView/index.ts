import ListViewEvent from './ListViewEvent';
import View, { ViewOptions, ViewClass } from '../../View';

export { ListViewEvent };

export interface ListViewOptions extends ViewOptions {
  container?: string | HTMLElement;
  selector?: string;
  itemClass?: ViewClass;
  itemOptions?: ViewOptions;
}

export default class ListView<TItem extends View = View> extends View {
  container: HTMLElement;
  items: TItem[] = [];
  itemClass: ViewClass;
  itemOptions: ViewOptions;

  constructor(options: ListViewOptions = {}) {
    super(options);

    const { selector, itemClass = View, itemOptions = {} } = options;
    const params = this.getParams(options);

    this.container = params.element({ name: 'container' }) || this.element;
    this.itemClass = itemClass;
    this.itemOptions = itemOptions;

    if (selector) {
      const { items, container } = this;
      const elements = container.querySelectorAll(selector);
      for (let index = 0; index < elements.length; index++) {
        const element = <HTMLElement>elements[index];
        const child = this.createItem(element);
        items.push(child);
      }
    }
  }

  addItem(item: TItem, at?: number): this {
    const { items, container } = this;
    const count = items.length;

    if (at === void 0 || at >= count) {
      at = count;
      container.appendChild(item.element);
      items.push(item);
    } else if (at <= 0) {
      at = 0;
      container.insertBefore(item.element, container.firstChild);
      items.unshift(item);
    } else {
      container.insertBefore(item.element, items[at].element);
      items.splice(at, 0, item);
    }

    this.handleItemAdd(item, at);
    return this;
  }

  addItems(children: TItem[], at?: number): this {
    for (let index = 0, count = children.length; index < count; index++) {
      this.addItem(children[index], at ? at + index : at);
    }

    return this;
  }

  find(callback: { (item: TItem, index: number): boolean }): TItem | undefined {
    return this.items.find(callback);
  }

  forEach(callback: { (item: TItem, index: number): void }) {
    this.items.forEach(callback);
  }

  getItem(index: number): TItem | null {
    return this.items[index] || null;
  }

  getLength(): number {
    return this.items.length;
  }

  indexOf(item: TItem): number {
    return this.items.indexOf(item);
  }

  removeAllItems(): this {
    const { items } = this;
    let item: TItem | undefined;

    while ((item = items.pop())) {
      this.handleItemRemove(item, items.length);
      item.dispose();
    }

    items.length = 0;
    return this;
  }

  removeItem(item: TItem): this {
    const { items } = this;
    const index = items.indexOf(item);
    if (index !== -1) {
      items.splice(index, 1);
      item.dispose();
      this.handleItemRemove(item, index);
    }

    return this;
  }

  removeItems(items: TItem[]): this {
    items.forEach(item => this.removeItem(item));
    return this;
  }

  protected createItem(element: HTMLElement): TItem {
    const { itemClass: viewClass, itemOptions: viewOptions } = this;
    return new viewClass({
      ...viewOptions,
      appendTo: this.container,
      element,
      parent: this,
    }) as TItem;
  }

  protected handleItemAdd(item: TItem, index: number) {
    this.emit(
      new ListViewEvent({
        index,
        item,
        target: this,
        type: ListViewEvent.addEvent,
      })
    );
  }

  protected handleItemRemove(item: TItem, index: number) {
    this.emit(
      new ListViewEvent({
        index,
        item,
        target: this,
        type: ListViewEvent.removeEvent,
      })
    );
  }
}

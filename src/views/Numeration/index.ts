import CycleableView, { CycleableViewEvent } from '../CycleableView';
import NumerationEvent from './NumerationEvent';
import View, { ViewOptions } from '../../View';
import { ListViewEvent } from '../ListView';
import { DelegatedEvent } from '../../Delegate';
import { IntervalType } from '../../types';

export { NumerationEvent };

/**
 * Constructor options for the Numeration class.
 */
export interface NumerationOptions extends ViewOptions {
  container?: HTMLElement | string;
  itemClass?: string;
  itemTagName?: string;
  selectedItemClass?: string;
  target?: CycleableView;
}

export default class Numeration extends View {
  container: HTMLElement;
  itemClass: string;
  items: HTMLElement[] = [];
  itemTagName: string;
  selectedItemClass: string;
  selectedRange: IntervalType = { min: -1, max: -1 };
  view: CycleableView | undefined;

  constructor(options: NumerationOptions = {}) {
    super({
      className: `${View.classNamePrefix}Numeration`,
      tagName: 'ul',
      ...options,
    });

    const {
      itemClass = `${View.classNamePrefix}Numeration--item`,
      itemTagName = 'li',
      parent,
      selectedItemClass = 'selected',
      target,
    } = options;

    this.container = this.getParams(options).element({
      name: 'container',
      defaultValue: this.element,
    });

    this.itemClass = itemClass;
    this.itemTagName = itemTagName;
    this.selectedItemClass = selectedItemClass;

    if (target instanceof CycleableView) {
      this.setView(target);
    } else if (parent instanceof CycleableView) {
      this.setView(parent);
    }

    this.delegate('click', this.handleClick, { selector: `.${itemClass}` });
  }

  handleClick(event: DelegatedEvent) {
    const index = this.items.indexOf(<any>event.delegateTarget);
    if (index !== -1) {
      this.handleSelectIndex(index);
    }

    return true;
  }

  handleCurrentChanged() {
    if (this.view) {
      this.setSelectedIndex(this.view.getCurrentIndex());
    }
  }

  handleLengthChanged() {
    if (this.view) {
      this.setLength(this.view.getLength());
    }
  }

  handleSelectIndex(index: number) {
    if (this.view) {
      this.view.setCurrentIndex(index);
    } else {
      this.emit(
        new NumerationEvent({
          index,
          target: this,
          type: NumerationEvent.changeEvent,
        })
      );
    }
  }

  setView(view: CycleableView): this {
    if (this.view === view) return this;

    if (this.view) {
      this.stopListening(this.view);
    }

    if (view) {
      this.setLength(view.getLength())
        .setSelectedIndex(view.getCurrentIndex())
        .listenTo(
          view,
          CycleableViewEvent.changeEvent,
          this.handleCurrentChanged
        )
        .listenTo(view, ListViewEvent.addEvent, this.handleLengthChanged)
        .listenTo(view, ListViewEvent.removeEvent, this.handleLengthChanged);
    }

    this.view = view;
    return this;
  }

  setLength(length: number): this {
    const { container, itemClass, items, itemTagName } = this;
    let itemsLength = items.length;

    if (itemsLength == length) {
      return this;
    }

    while (itemsLength < length) {
      let item = document.createElement(itemTagName);
      item.className = itemClass;
      item.innerText = `${itemsLength + 1}`;

      items.push(item);
      itemsLength += 1;
      container.appendChild(item);
    }

    while (itemsLength > length) {
      let child = items.pop();
      if (child) {
        container.removeChild(child);
      }

      itemsLength -= 1;
    }

    this.setSelected(this.selectedRange);
    return this;
  }

  setSelectedIndex(index: number): this {
    return this.setSelected({ min: index, max: index });
  }

  setSelected(range: IntervalType): this {
    const { items, selectedRange, selectedItemClass } = this;
    const length = items.length - 1;
    let { min, max } = range;
    min = Math.max(-1, Math.min(length, Math.floor(min)));
    max = Math.max(-1, Math.min(length, Math.ceil(max)));

    if (max < min) {
      let tmp = min;
      min = max;
      max = tmp;
    }

    if (selectedRange.min == min && selectedRange.max == max) {
      return this;
    }

    for (let index = 0; index <= length; index++) {
      items[index].classList.toggle(
        selectedItemClass,
        index >= min && index <= max
      );
    }

    selectedRange.min = min;
    selectedRange.max = max;
    return this;
  }
}

import dissolve from '../../fx/transitions/dissolve';
import ImageRatios, { RatioSet, Ratio } from '../ImageRatios';
import Params from '../../Params';
import { ViewOptions } from '../../View';

export interface PictureOptions extends ViewOptions {
  disableResize?: boolean;
  disableVisibility?: boolean;
}

export default class Picture extends ImageRatios {
  constructor(options: PictureOptions) {
    super({
      transition: dissolve({
        duration: 200,
      }),
      ...options,
    });
  }

  getRatioSet(params: Params): RatioSet {
    const sources = this.queryAll<HTMLSourceElement>('source');
    const focusX = params.number({ name: 'focusX', defaultValue: 0.5 });
    const focusY = params.number({ name: 'focusY', defaultValue: 0.5 });

    const ratios = sources.map(
      source =>
        new Ratio({
          focusX,
          focusY,
          height: parseInt(`${source.getAttribute('data-height')}`),
          sourceSet: `${
            source.hasAttribute('data-srcset')
              ? source.getAttribute('data-srcset')
              : source.getAttribute('srcset')
          }`,
          width: parseInt(`${source.getAttribute('data-width')}`),
        })
    );

    return new RatioSet(ratios);
  }

  handleResize() {
    const { content, displayHeight, displayWidth } = this;
    if (content) {
      content.setDisplaySize(displayWidth, displayHeight);
    }

    this.debouncedUpdate();
  }
}

import View from '../../View';
import whenViewLoaded from '../../utils/whenViewLoaded';
import { Transition } from '../../fx/transitions';

export interface SequencerOptions<
  TOptions extends SequenceOptions = SequenceOptions
> {
  callbackContext?: any;
  dismissCallback?: SequencerCallback<TOptions> | null;
  endCallback?: SequencerCallback<TOptions> | null;
  startCallback?: SequencerCallback<TOptions> | null;
}

export interface SequenceOptions<TView extends View = View> {
  from: TView | null;
  to: TView | null;
  transition: Transition;
}

export type SequencerCallback<
  TOptions extends SequenceOptions = SequenceOptions
> = (options: TOptions) => void;

export default class Sequencer<
  TOptions extends SequenceOptions = SequenceOptions
> {
  callbackContext: any = null;
  dismissCallback: SequencerCallback<TOptions> | null = null;
  endCallback: SequencerCallback<TOptions> | null = null;
  startCallback: SequencerCallback<TOptions> | null = null;

  protected sequence: Promise<any> | null = null;
  protected shelved: TOptions | null = null;

  constructor(options: SequencerOptions<TOptions> = {}) {
    Object.assign(this, options);
  }

  inTransition(): boolean {
    return !!this.sequence;
  }

  transist(options: TOptions) {
    const { callbackContext, dismissCallback, sequence } = this;

    if (sequence) {
      if (this.shelved && dismissCallback) {
        dismissCallback.call(callbackContext, this.shelved);
      }
      this.shelved = options;
    } else {
      this.sequence = this.createSequence(options);
    }
  }

  protected createSequence(options: TOptions): Promise<any> {
    const { transition, from, to } = options;
    const fromElement = from ? from.element : undefined;
    const toElement = to ? to.element : undefined;

    return whenViewLoaded(to)
      .then(() => {
        this.handleTransitionStart(options);
        return transition(fromElement, toElement);
      })
      .then(() => this.handleTransitionEnd(options));
  }

  protected handleTransitionEnd(options: TOptions) {
    const { callbackContext, shelved, endCallback } = this;
    const { from, to } = options;
    this.shelved = null;

    if (from) {
      from.removeClass('sequenceFrom');
    }

    if (to) {
      to.removeClass('sequenceTo');
    }

    if (shelved) {
      shelved.from = options.to;
      this.sequence = this.createSequence(shelved);
    } else {
      this.sequence = null;
    }

    if (endCallback) {
      endCallback.call(callbackContext, options);
    }
  }

  protected handleTransitionStart(options: TOptions) {
    const { from, to } = options;
    const { callbackContext, startCallback } = this;
    if (startCallback) {
      startCallback.call(callbackContext, options);
    }

    if (from) {
      from.addClass('sequenceFrom');
    }

    if (to) {
      to.addClass('sequenceTo');
    }
  }
}

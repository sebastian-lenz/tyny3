import Event, { EventOptions } from '../../Event';
import Slideshow from './index';
import View from '../../View';

export interface SlideshowEventOptions extends EventOptions<Slideshow> {
  from: View | null;
  to: View | null;
}

export default class SwapEvent extends Event<Slideshow> {
  readonly from: View | null;
  readonly to: View | null;

  static transitionDismissEvent: string = 'transitionDismiss';
  static transitionEndEvent: string = 'transitionEnd';
  static transitionStartEvent: string = 'transitionStart';

  constructor(options: SlideshowEventOptions) {
    super(options);
    this.from = options.from;
    this.to = options.to;
  }
}

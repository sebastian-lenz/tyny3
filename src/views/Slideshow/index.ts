import CycleableView, { CycleableViewOptions } from '../CycleableView';
import dissolve from '../../fx/transitions/dissolve';
import isSelectableView from '../../utils/isSelectableView';
import Sequencer, { SequenceOptions } from './Sequencer';
import SlideshowEvent from './SlideshowEvent';
import View from '../../View';
import { Transition } from '../../fx/transitions';

export { Sequencer, SequenceOptions, Slideshow, SlideshowEvent };

export interface SlideshowOptions extends CycleableViewOptions {
  transition?: Transition;
}

export interface SlideshowTransitionOptions {
  transition?: Transition;
}

export default class Slideshow<
  TChild extends View = View
> extends CycleableView<TChild, SlideshowTransitionOptions> {
  transition: Transition;
  protected sequencer: Sequencer<SequenceOptions<TChild>>;

  constructor(options: SlideshowOptions = {}) {
    super({
      className: `${View.classNamePrefix}Slideshow`,
      isLooped: true,
      ...options,
    });

    const sequencer = new Sequencer<SequenceOptions<TChild>>({
      callbackContext: this,
      dismissCallback: this.handleTransitionDismiss,
      endCallback: this.handleTransitionEnd,
      startCallback: this.handleTransitionStart,
    });

    this.transition = options.transition || dissolve();
    this.sequencer = sequencer;
  }

  handleTransition(
    from: TChild | null,
    to: TChild | null,
    options: SlideshowTransitionOptions = {}
  ) {
    const { transition, sequencer } = this;
    sequencer.transist({ transition, from, to, ...options });
  }

  handleTransitionDismiss({ from, to }: SequenceOptions<TChild>) {
    this.emit(
      new SlideshowEvent({
        from,
        target: <any>this,
        to,
        type: SlideshowEvent.transitionDismissEvent,
      })
    );
  }

  handleTransitionStart({ from, to }: SequenceOptions<TChild>) {
    if (isSelectableView(from)) {
      from.setSelected(false);
    }

    if (isSelectableView(to)) {
      to.setSelected(true);
    }

    this.emit(
      new SlideshowEvent({
        from,
        target: <any>this,
        to,
        type: SlideshowEvent.transitionStartEvent,
      })
    );
  }

  handleTransitionEnd({ from, to }: SequenceOptions<TChild>) {
    this.emit(
      new SlideshowEvent({
        from,
        target: <any>this,
        to,
        type: SlideshowEvent.transitionEndEvent,
      })
    );
  }

  inTransition(): boolean {
    return this.sequencer.inTransition();
  }

  setCurrentImmediate(value: TChild) {
    this.setCurrent(value, { transition: () => Promise.resolve() });
  }
}

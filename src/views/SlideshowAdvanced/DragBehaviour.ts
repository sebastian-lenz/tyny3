import DragBehaviourBase from '../../pointers/behaviours/DragBehaviour';
import DragEvent from '../../pointers/behaviours/DragEvent';
import easeOutExpo from '../../fx/easings/easeOutExpo';
import PointerList from '../../pointers/PointerList';
import stop from '../../fx/stop';
import tween from '../../fx/tween';
import View from '../../View';

export { DragEvent };

export interface DragBehaviourView<T = any> extends View {
  disabled?: boolean;
  offset: number;
  viewportWidth: number;

  getNext(): T;
  getPrevious(): T;
  inTransition(): boolean;
  setAllowTransition(value: boolean): void;
  setCurrentImmediate(value: T): void;
  setOffset(value: number): void;
}

export interface DragBehaviourOptions {
  disableMouseDrag?: boolean;
  view: DragBehaviourView;
}

export default class DragBehaviour extends DragBehaviourBase {
  disableMouseDrag: boolean;
  initialOffset: number = 0;
  preventNextClick: false | number = false;
  view: DragBehaviourView;

  constructor(options: DragBehaviourOptions) {
    super(PointerList.forView(options.view));

    const { disableMouseDrag, view } = options;
    this.direction = 'horizontal';
    this.disableMouseDrag = !!disableMouseDrag;
    this.view = view;

    view.delegate('click', this.handleClick, { capture: true, scope: this });

    this.listenTo(this, DragEvent.dragStartEvent, this.handleDragStart)
      .listenTo(this, DragEvent.dragEvent, this.handleDrag)
      .listenTo(this, DragEvent.dragEndEvent, this.handleDragEnd);
  }

  handleClick(event: Event) {
    if (this.preventNextClick) {
      window.clearTimeout(this.preventNextClick);
      event.preventDefault();
      event.stopPropagation();
      this.preventNextClick = false;
    }
  }

  handleDragStart(event: DragEvent) {
    const { disableMouseDrag, view } = this;
    if (
      view.disabled ||
      view.inTransition() ||
      (disableMouseDrag && event.listEvent.pointer.type === 'mouse')
    ) {
      event.preventDefault();
      return;
    }

    const { nativeEvent } = event.listEvent;
    if (nativeEvent) {
      nativeEvent.preventDefault();
    }

    stop(view);
    this.initialOffset = view.offset;
    view.setAllowTransition(false);
  }

  handleDrag(event: DragEvent) {
    const { initialOffset, view } = this;
    const { nativeEvent, pointer } = event.listEvent;
    if (nativeEvent) {
      nativeEvent.preventDefault();
    }

    const { viewportWidth } = view;
    const offset = initialOffset + pointer.getDelta().x / viewportWidth;
    view.setOffset(offset);
  }

  handleDragEnd(event: DragEvent) {
    const { view } = this;
    const { offset } = view;
    const { clientX } = event.listEvent.pointer.velocity.get();

    if (this.preventNextClick) window.clearTimeout(this.preventNextClick);
    this.preventNextClick = window.setTimeout(
      () => (this.preventNextClick = false),
      200
    );

    if (Math.abs(clientX) > 5) {
      this.step(clientX > 0);
    } else if (Math.abs(offset) > 0.4) {
      this.step(offset > 0);
    }

    const onFinished = () => {
      view.setOffset(0);
      view.setAllowTransition(true);
    };

    if (view.offset === 0) {
      return onFinished();
    }

    tween(
      view,
      {
        offset: 0,
      },
      {
        duration: 300,
        easing: easeOutExpo,
      }
    ).then(onFinished);
  }

  step(backward?: boolean) {
    const { view } = this;
    const { offset } = view;
    const slide = backward ? view.getPrevious() : view.getNext();

    view.setCurrentImmediate(slide);
    view.setOffset(backward ? offset - 1 : offset + 1);
  }
}

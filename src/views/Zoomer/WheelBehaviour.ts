import normalizeWheel from '../../utils/normalizeWheel';
import spring from '../../fx/spring';
import wheelProps from '../../utils/vendors/wheelProps';
import Zoomer from './index';

const power = 1;

export default class WheelBehaviour {
  zoomer: Zoomer;

  constructor(zoomer: Zoomer) {
    this.zoomer = zoomer;

    const { eventName } = wheelProps();
    zoomer.delegate(eventName, this.handleWheel, {
      scope: this,
    });
  }

  handleWheel(event: WheelEvent) {
    const data = normalizeWheel(event);
    const { zoomer } = this;
    const {
      position: { x: currentX, y: currentY },
      scale: currentScale,
    } = zoomer;

    let scale = currentScale;
    if (data.spinY < -0.01) {
      scale /= 1 - data.spinY * power;
    } else if (data.spinY > 0.01) {
      scale *= 1 + data.spinY * power;
    }

    scale = zoomer.limitScale(scale);
    if (Math.abs(currentScale - scale) < 0.0001) {
      return;
    }

    const { left, top } = zoomer.element.getBoundingClientRect();
    let x = event.clientX - left;
    let y = event.clientY - top;
    x += ((currentX - x) / currentScale) * scale;
    y += ((currentY - y) / currentScale) * scale;

    spring(
      zoomer,
      {
        position: zoomer.limitPosition({ x, y }, scale),
        scale,
      },
      {
        epsilon: 0.0001,
      }
    );
  }
}
